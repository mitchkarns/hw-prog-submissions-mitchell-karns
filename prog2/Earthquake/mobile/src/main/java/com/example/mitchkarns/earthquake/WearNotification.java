package com.example.mitchkarns.earthquake;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;

/**
 * Created by mitch.karns on 10/14/15.
 */
public class WearNotification extends WearableActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();
    }
}
