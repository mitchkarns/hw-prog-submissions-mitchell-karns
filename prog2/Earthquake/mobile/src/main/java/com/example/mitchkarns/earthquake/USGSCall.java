package com.example.mitchkarns.earthquake;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by mitch.karns on 10/13/15.
 */
public class USGSCall extends AsyncTask<String, Void, String> {

    InputStream inputStream = null;
    String result = "";

    protected String doInBackground(String... params) {
        for (String endpoint : params) {

            try {
                URL url = new URL(endpoint);
                URLConnection ukr = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(ukr.getInputStream()));
                String inputLine;
                String outdata = "";
                while ((inputLine = in.readLine()) != null)
                    outdata += inputLine;
                in.close();
                return outdata;
            } catch (IllegalStateException e3) {
                Log.e("IllegalStateException", e3.toString());
                e3.printStackTrace();
            } catch (IOException e4) {
                Log.e("IOException", e4.toString());
                e4.printStackTrace();
            }
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        Log.v("this is the result", result);
    }
}
