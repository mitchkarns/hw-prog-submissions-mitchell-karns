package com.example.mitchkarns.earthquake;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private String prevEarthquakeId;
    private JSONObject prevEarthquake;
    protected JSONArray prevEarthquakeLoc;
    private double eqLat;
    private double eqLong;
    private GoogleApiClient mGoogleApiClient;
    private android.location.Location mLastLocation;
    public static String TAG = "GPSActivity";
    public static int UPDATE_INTERVAL_MS = 60000;
    public static int FASTEST_INTERVAL_MS = 1000;
    //in meters
    private float distance = -1;
    private String magn;
    private String loc;
    private View front;
    private ImageView mainImageView;
    private TableLayout allQuakes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        front = findViewById(R.id.front);
        mainImageView = (ImageView)findViewById(R.id.images);
        allQuakes = (TableLayout) findViewById(R.id.allQuakes);
        front.bringToFront();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)  // used for data layer API
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        //mGoogleApiClient.connect();

        prevEarthquakeId = "";
        prevEarthquake = null;

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                new USGSCall().execute("http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit=1&orderby=time");
            }
        };
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 1000);
    }


    public void checkQuake(JSONObject json, String id){
        try {
            if (!prevEarthquakeId.equals(id) && prevEarthquake != null) {
                String mag = ((JSONObject) json.get("properties")).getString("mag");
                String location = ((JSONObject) json.get("properties")).getString("place");
                JSONArray coords = (JSONArray)((JSONObject) json.get("geometry")).get("coordinates");
                this.magn = mag;
                this.loc = location;
                System.out.println(mag);
                System.out.println(coords);
                System.out.println(location);
                System.out.println(id);
                prevEarthquakeLoc = coords;
                prevEarthquake = json;
                prevEarthquakeId = id;
                onLocationChanged(mLastLocation);
                sendNotification(location);
                newQuake();
            }
            else if (prevEarthquake == null) {
                System.out.println("changing old eq");
                String mag = ((JSONObject) json.get("properties")).getString("mag");
                String location = ((JSONObject) json.get("properties")).getString("place");
                JSONArray coords = (JSONArray)((JSONObject) json.get("geometry")).get("coordinates");
                this.magn = mag;
                this.loc = location;
                System.out.println(mag);
                System.out.println(coords);
                System.out.println(location);
                System.out.println(id);
                prevEarthquakeLoc = coords;
                prevEarthquake = json;
                prevEarthquakeId = id;
                onLocationChanged(mLastLocation);
                sendNotification(location);
                newQuake();
            }
            eqLong = prevEarthquakeLoc.getDouble(0);
            eqLat = prevEarthquakeLoc.getDouble(1);
            onLocationChanged(mLastLocation);
        }catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void onLocationChanged(Location location) {
        //(latitude, longitude)
        mLastLocation = location;
            if (prevEarthquakeLoc != null && mLastLocation != null) {
                Location temp = new Location("");
                temp.setLatitude(eqLat);
                temp.setLongitude(eqLong);
                distance = mLastLocation.distanceTo(temp);
            }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_MS)
                .setFastestInterval(FASTEST_INTERVAL_MS);

        LocationServices.FusedLocationApi
                .requestLocationUpdates(mGoogleApiClient, locationRequest, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.getStatus().isSuccess()) {
                            Log.d(TAG, "Successfully requested");
                        } else {
                            Log.e(TAG, status.getStatusMessage());
                        }
                    }
                });
    }
    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }
    @Override
    public void onConnectionSuspended(int i){
    }
    @Override
    public void onConnectionFailed(ConnectionResult r){
    }

    private class USGSCall extends AsyncTask<String, Void, String> {

        InputStream inputStream = null;
        String result = "";

        protected String doInBackground(String... params) {
            for (String endpoint : params) {

                try {
                    URL url = new URL(endpoint);
                    URLConnection ukr = url.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(ukr.getInputStream()));
                    String inputLine;
                    String outdata = "";
                    while ((inputLine = in.readLine()) != null)
                        outdata += inputLine;
                    in.close();
                    return outdata;
                } catch (IllegalStateException e3) {
                    Log.e("IllegalStateException", e3.toString());
                    e3.printStackTrace();
                } catch (IOException e4) {
                    Log.e("IOException", e4.toString());
                    e4.printStackTrace();
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray features = jsonObject.getJSONArray("features");
                JSONObject feats = (JSONObject) features.get(0);
                checkQuake(feats, feats.getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class InstagramImage extends AsyncTask<String, Void, String> {

        InputStream inputStream = null;
        String result = "";

        protected String doInBackground(String... params) {
            for (String endpoint : params) {

                try {
                    URL url = new URL(endpoint);
                    URLConnection ukr = url.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(ukr.getInputStream()));
                    String inputLine;
                    String outdata = "";
                    while ((inputLine = in.readLine()) != null)
                        outdata += inputLine;
                    in.close();
                    return outdata;
                } catch (IllegalStateException e3) {
                    Log.e("IllegalStateException", e3.toString());
                    e3.printStackTrace();
                } catch (IOException e4) {
                    Log.e("IOException", e4.toString());
                    e4.printStackTrace();
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                int l = jsonObject.getJSONArray("data").length();
                System.out.println("this is l: " + l);
                int i = 0;
                if (l > 0) {
                    Random r = new Random();
                    i = r.nextInt(l);
                    System.out.println(i);
                    JSONObject data = (JSONObject) jsonObject.getJSONArray("data").get(i);
                    JSONObject images = (JSONObject)((JSONObject) data.get("images")).get("standard_resolution");
                    String img_url = (String) images.get("url");
                    new ImageLoadTask(img_url, mainImageView).execute();
                    System.out.println(img_url);
                }
                else{
                    mainImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendNotification(String location){
        System.out.println("pressing button");
        int notificationId = 101;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, WearNotification.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);

        //Building notification layout
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle((int) (distance/1000) + "km")
                        .setContentText(location)
                        .setContentIntent(viewPendingIntent);

        // instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        // Build the notification and notify it using notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public void findImage(View v){
        String url = "";
        url = "https://api.instagram.com/v1/media/search?lat="+eqLat+"&lng="+eqLong+"&distance=5000&client_id=89e1c00f94c14e7c8642c79d3e0fb9db";
        System.out.println(url);
        new InstagramImage().execute(url);
        mainImageView.bringToFront();
        front.bringToFront();
        allQuakes.setVisibility(View.INVISIBLE);
        mainImageView.setVisibility(View.VISIBLE);
        //ImageLoadTask im = new ImageLoadTask(url, mainImageView);
        //im.execute();
    }

    public void viewQuakes(View v){
        mainImageView.setVisibility(View.INVISIBLE);
        allQuakes.setVisibility(View.VISIBLE);
        allQuakes.bringToFront();
        front.bringToFront();
    }

    public void newQuake(){
        if (allQuakes.getChildCount() > 9)
            allQuakes.removeViewAt(9);
        TableRow row= new TableRow(this);
        TextView t = new TextView(this);
        TextView m = new TextView(this);
        t.setText(this.loc);
        TextView d = new TextView(this);
        m.setText("Mag: " + this.magn);
        d.setText((int) (this.distance/1000) + "km");
        d.setGravity(Gravity.RIGHT);
        m.setGravity(Gravity.CENTER);
        m.setPadding(3, 3, 3, 3);
        t.setPadding(3, 3, 3, 3);
        d.setPadding(3, 3, 3, 3);
        m.setTextColor(Color.BLACK);
        t.setTextColor(Color.BLACK);
        d.setTextColor(Color.BLACK);
        row.addView(t);
        row.addView(m);
        row.addView(d);
        allQuakes.addView(row, 0);
    }

}
