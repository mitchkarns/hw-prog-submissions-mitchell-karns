package com.example.mitchkarns.youanimal;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    private double[] mConversions = {1.0, 2.0, 3.2, 3.64, 20, 1.78, 8.89};
    private String[] mTexts = {"Human", "Bear", "Cat", "Dog","Hamster", "Hippo", "Kangaroo"};
    private int mYears = 0;
    private View[] mInputViews;
    private int mInput = 0;
    private int mOutput = 0;
    //private ValueAnimator mColorAnimationIn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set edit text function
        EditText e = ((EditText)findViewById(R.id.Input));
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println(s.length());
                if(s.length() != 0)
                    mYears = Integer.parseInt(s.toString());
                else {
                    mYears = 0;
                }
                computeOutput();
            }
        });

        mInputViews = new View[]{findViewById(R.id.humuanIn),findViewById(R.id.bearIn),findViewById(R.id.catIn),
                findViewById(R.id.dogIn),findViewById(R.id.hamsterIn),findViewById(R.id.hippoIn),findViewById(R.id.kangarooIn)};

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();

    }

    //for fading out the input images
    private void fadeOut1(){
        final View v;
        if(mInput == 0)
            v = findViewById(R.id.humuanIn);
        else if(mInput == 1)
            v = findViewById(R.id.bearIn);
        else if(mInput == 2)
            v = findViewById(R.id.catIn);
        else if(mInput == 3)
            v = findViewById(R.id.dogIn);
        else if(mInput == 4)
            v = findViewById(R.id.hamsterIn);
        else if(mInput == 5)
            v = findViewById(R.id.hippoIn);
        else
            v = findViewById(R.id.kangarooIn);

        Integer colorFrom = Color.parseColor("#0099cc");
        Integer colorTo = Color.BLACK;
        ValueAnimator cAm = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        cAm.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                v.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        cAm.start();

    }

    //for fading out the output images
    private void fadeOut2(){
        final View v;
        if(mOutput == 0)
            v = findViewById(R.id.humuanOut);
        else if(mOutput == 1)
            v = findViewById(R.id.bearOut);
        else if(mOutput == 2)
            v = findViewById(R.id.catOut);
        else if(mOutput == 3)
            v = findViewById(R.id.dogOut);
        else if(mOutput == 4)
            v = findViewById(R.id.hamsterOut);
        else if(mOutput == 5)
            v = findViewById(R.id.hippoOut);
        else
            v = findViewById(R.id.kangarooOut);

        Integer colorFrom = Color.parseColor("#0099cc");
        Integer colorTo = Color.BLACK;
        ValueAnimator cAm = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        cAm.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                v.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        cAm.start();

    }

    //for making the images turn blue when clicked
    public void colorChange(final View view){
        Integer colorFrom = Color.BLACK;
        Integer colorTo = Color.parseColor("#0099cc");
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                view.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void computeOutput(){
        TextView v = (TextView)findViewById(R.id.InAnimal);
        String output = mTexts[mInput] + " years:";
        v.setText(output);

        double y = mYears * (mConversions[mOutput]/mConversions[mInput]);
        output = "" + String.format("%.2f", y);
        v = (TextView)findViewById(R.id.OutputYears);
        v.setText(output);

        v = (TextView)findViewById(R.id.OutAnimal);
        output = mTexts[mOutput] + " years:";
        v.setText(output);
    }

    public void humanInput(View view){
        fadeOut1();
        mInput = 0;
        colorChange(view);
        computeOutput();
    }

    public void humanOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 0;
        computeOutput();
    }

    public void bearInput(View view){
        fadeOut1();
        mInput = 1;
        colorChange(view);
        computeOutput();
    }

    public void bearOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 1;
        computeOutput();
    }

    public void catInput(View view){
        fadeOut1();
        mInput = 2;
        colorChange(view);
        computeOutput();
    }

    public void catOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 2;
        computeOutput();
    }

    public void dogInput(View view){
        fadeOut1();
        mInput = 3;
        colorChange(view);
        computeOutput();
    }

    public void dogOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 3;
        computeOutput();
    }

    public void hamsterInput(View view){
        fadeOut1();
        mInput = 4;
        colorChange(view);
        computeOutput();
    }

    public void hamsterOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 4;
        computeOutput();
    }

    public void hippoInput(View view){
        fadeOut1();
        mInput = 5;
        colorChange(view);
        computeOutput();
    }

    public void hippoOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 5;
        computeOutput();
    }

    public void kangarooInput(View view){
        fadeOut1();
        mInput = 6;
        colorChange(view);
        computeOutput();
    }

    public void kangarooOutput(View view){
        fadeOut2();
        colorChange(view);
        mOutput = 6;
        computeOutput();
    }
}
